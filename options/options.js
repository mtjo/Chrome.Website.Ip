var config = {
    host: ""
};

$(document).ready(restore_options)
$('#save-button').click(save_options);

function restore_options() {
    chrome.storage.sync.get({
        autohidden: true,
    }, function (items) {
        document.getElementById('autohidden').checked = items.autohidden;
    });
}

function save_options() {
    var autohidden = document.getElementById('autohidden').checked;
    chrome.storage.sync.set({
        autohidden: autohidden
    }, function () {
        $('#notify').css('display', "block");
        setTimeout(function () {
            $('#notify').css('display', "none");
        }, 1500);
    });
}

var manifestData = chrome.runtime.getManifest();
var currentVersion = manifestData.version;
console.log(currentVersion);
$('#version-info').html(currentVersion);
